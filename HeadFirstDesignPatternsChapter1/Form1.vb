﻿Public Class Form1

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim mallard As Duck
        mallard = New MallardDuck
        With mallard
            .performFly()
            .performQuack()
            '.setFlyBehavior = New FlyNoWay
            '.setQuackBehavior = New MuteQuack
            '.performFly()
            '.performQuack()
        End With

        Dim model As Duck
        model = New ModelDuck
        With model
            .performFly()
            .performQuack()
            .setFlyBehavior = New FlyRocketPowered
            .performFly()
        End With

    End Sub
End Class
