﻿#Region "Duck Implementations"
'***************************************************************Specific Duck Class called MallardDuck
Public Class DecoyDuck
    Inherits Duck

    Sub New()
        quackBehavior = New Quack()
        flyBehavior = New FlyNoWay()
    End Sub

    Public Overrides Sub display()
        MsgBox("I am a decoy duck")
    End Sub
End Class

'***************************************************************Specific Duck Class called MallardDuck
Public Class Rubberduck
    Inherits Duck

    Sub New()
        quackBehavior = New Quack()
        flyBehavior = New FlyNoWay()
    End Sub

    Public Overrides Sub display()
        MsgBox("I am a rubber duck")
    End Sub
End Class

'***************************************************************Specific Duck Class called MallardDuck
Public Class RedheadDuck
    Inherits Duck

    Sub New()
        quackBehavior = New Quack()
        flyBehavior = New FlyNoWay()
    End Sub

    Public Overrides Sub display()
        MsgBox("I am a readhead Duck")
    End Sub
End Class
'***************************************************************Specific Duck Class called MallardDuck
Public Class ModelDuck
    Inherits Duck

    Sub New()
        quackBehavior = New Quack()
        flyBehavior = New FlyNoWay()
    End Sub

    Public Overrides Sub display()
        MsgBox("I am a model duck")
    End Sub
End Class

'***************************************************************Specific Duck Class called MallardDuck
Public Class MallardDuck
    Inherits Duck

    Sub New()
        quackBehavior = New Quack()
        flyBehavior = New FlyWithWings()
    End Sub

    Public Overrides Sub display()
        MsgBox("I am a real Mallard Duck")
    End Sub
End Class
#End Region

#Region "Duck Motherclass"
''***************************************************************Duck Class
Public MustInherit Class Duck
    Protected flyBehavior As FlyBehavior
    Protected quackBehavior As QuackBehavior

    Sub performFly()
        flyBehavior.fly()
    End Sub

    Sub performQuack()
        quackBehavior.quack()
    End Sub

    Public WriteOnly Property setFlyBehavior As FlyBehavior
        Set(value As FlyBehavior)
            flyBehavior = value
        End Set
    End Property

    Public WriteOnly Property setQuackBehavior As QuackBehavior
        Set(value As QuackBehavior)
            quackBehavior = value
        End Set
    End Property


    Sub swim()
        MsgBox("All ducks float, even decoys!")
    End Sub

    MustOverride Sub display()

End Class
#End Region

#Region "Encapsulated Duck Behavior (Interfaces)"
Public Interface FlyBehavior
    Sub fly()
End Interface

Public Interface QuackBehavior
    Sub quack()
End Interface
#End Region

#Region "FlyBehavior Classes (Implementations)"
Public Class FlyWithWings
    Implements FlyBehavior

    Public Sub fly() Implements FlyBehavior.fly
        MsgBox("I am flying!!")
    End Sub
End Class

Public Class FlyNoWay
    Implements FlyBehavior

    Public Sub fly() Implements FlyBehavior.fly
        MsgBox("I cannot fly")
    End Sub
End Class

Public Class FlyRocketPowered
    Implements FlyBehavior

    Public Sub fly() Implements FlyBehavior.fly
        MsgBox("I am flying with a rocket!")
    End Sub
End Class
#End Region

#Region "QuackBehavior Classes (Implementations)"
Public Class Quack
    Implements QuackBehavior

    Public Sub quack() Implements QuackBehavior.quack
        MsgBox("Quarck")
    End Sub
End Class

Public Class Squeak
    Implements QuackBehavior

    Public Sub quack() Implements QuackBehavior.quack
        MsgBox("Squeak")
    End Sub
End Class

Public Class MuteQuack
    Implements QuackBehavior

    Public Sub quack() Implements QuackBehavior.quack
        MsgBox("<<Silence>>")
    End Sub
End Class
#End Region